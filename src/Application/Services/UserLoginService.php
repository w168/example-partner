<?php

declare(strict_types=1);

namespace App\Application\Services;

use App\Domain\User\User;

final class UserLoginService
{
	private ?User $user = null;
	
	public function getLoggedInUser(): ?User
	{
		if (empty($_SESSION['user']['_id']))
		{
			return null;
		}
		if (!$this->user)
		{
			$this->user = User::fromArray($_SESSION['user']);
		}
		return $this->user;
	}
	
	public function loginUser(User $user)
	{
		$_SESSION['user'] = $user->toArray();
	}
	
	public function logout()
	{
		session_destroy();
	}
}
