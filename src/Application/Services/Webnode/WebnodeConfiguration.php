<?php

declare(strict_types=1);

namespace App\Application\Services\Webnode;

final class WebnodeConfiguration
{
	private string $public;

	private string $secret;

	private string $portalAuthorizationUrl;

	private string $oauth2AuthorizationUrl;

	public function __construct(
		string $public,
		string $secret,
		string $portalAuthorizationUrl,
		string $oauth2AuthorizationUrl
	) {
		$this->public                 = $public;
		$this->secret                 = $secret;
		$this->portalAuthorizationUrl = $portalAuthorizationUrl;
		$this->oauth2AuthorizationUrl = $oauth2AuthorizationUrl;
	}
	

	public function getPublic(): string
	{
		return $this->public;
	}

	public function getSecret(): string
	{
		return $this->secret;
	}

	public function getPortalAuthorizationUrl(): string
	{
		return $this->portalAuthorizationUrl;
	}

	public function getOauth2AuthorizationUrl(): string
	{
		return $this->oauth2AuthorizationUrl;
	}
}
