<?php

declare(strict_types=1);

namespace App\Application\Services\Webnode;

use App\Application\Services\UserLoginService;
use App\Domain\Project;
use App\Domain\User\User;
use App\Domain\User\UserRepository;
use GuzzleHttp\Exception\GuzzleException;
use webnode\oauth2\Service\OAuth2ApiInterface;

final class WebnodeFacade
{
	private ProjectApiClientInterface $projectApi;

	private UserRepository $userRepository;

	private OAuth2ApiInterface $oauth2Api;

	private UserLoginService $userLoginService;

	public function __construct(
		ProjectApiClientInterface $projectApi,
		UserRepository $userRepository,
		OAuth2ApiInterface $oauth2Api,
		UserLoginService $userLoginService
	) {
		$this->projectApi           = $projectApi;
		$this->userRepository       = $userRepository;
		$this->oauth2Api            = $oauth2Api;
		$this->userLoginService     = $userLoginService;
	}
	
	
	private function setCacheValue(string $key, $data)
	{
		$_SESSION['wndCache'][$key] = $data;
	}
	
	
	/**
	 * @param string $key
	 * @return mixed|null
	 */
	private function getCacheValue(string $key)
	{
		return $_SESSION['wndCache'][$key] ?? null;
	}
	
	/**
	 * @return Project[]
	 */
	public function getUserProjects(?User $user = null): array
	{
		$user = $user ?? $this->userLoginService->getLoggedInUser();
		$userIdentifier = $user->getWebnodeIdentifier();
		if ($userIdentifier === null)
		{
			return [];
		}

		// you should store all projects data internally, this is just an example how to get them
		$cacheKey = $user->getId() . '|projects';
		if (empty($this->getCacheValue($cacheKey)))
		{
			try
			{
				$projectsData = $this->projectApi->findByUserIdentifier($userIdentifier);
			}
			catch (GuzzleException $e)
			{
				$projectsData = [];
			}
			foreach ($projectsData as $key => $projectData)
			{
				$packages = $this->projectApi->getProjectPackages($projectData['identifier']);
				foreach ($packages as $package)
				{
					if ($package['isActive'])
					{
						$projectsData[$key]['package'] =  $package['identifier'];
						break;
					}
				}

				$projectDomains = $this->projectApi->getProjectDomains($projectData['identifier']);
				foreach ($projectDomains as $domain)
				{
					if ($domain['isMasterDomain'])
					{
						$projectsData[$key]['masterDomain'] =  $domain['domainName'];
					}
					else
					{
						$projectsData[$key]['otherDomains'][] = $domain['domainName'];
					}
				}
			}
			$this->setCacheValue($cacheKey, $projectsData);
		}
		$projectsData = $this->getCacheValue($cacheKey);
		
		$projects = [];
		foreach ($projectsData as $projectData)
		{
			$projects[] = new Project(
				$projectData['identifier'],
				$userIdentifier,
				$projectData['domainName'],
				$projectData['package'] ?? null,
				$projectData['cmsUrl'],
				$projectData['portalUrl'],
				$projectData['projectUrl'],
				$projectData['status'],
				$projectData['serverIpAddress'],
				$projectData['masterDomain'] ?? $projectData['domainName'] ?? null,
				$projectData['otherDomains'] ?? []
			);
		}
		
		return $projects;
	}

	public function getUserProject(string $identifier, ?User $user = null): ?Project
	{
		$project = $this->loadUserProject($identifier, $user);
		if (!$project || $project->getStatus() === Project::STATUS_INITIALIZED)
		{
			$this->clearProjectsCache($user ?? $this->userLoginService->getLoggedInUser());
			$project = $this->loadUserProject($identifier, $user);
		}
		return $project;
	}

	private function loadUserProject(string $identifier, ?User $user): ?Project
	{
		// this is very unoptimized, but we are showing just a naive implementation
		$projects = $this->getUserProjects($user);
		foreach ($projects as $project)
		{
			if ($project->getIdentifier() === $identifier)
			{
				return $project;
			}
		}
		return null;
	}

	/**
	 * @return string (returns wizard URL)
	 */
	public function createProject(?string $projectIdentifier = null, ?User $user = null): string
	{
		$user = $user ?? $this->userLoginService->getLoggedInUser();
		$response = $this->projectApi->create($user->getEmail(), $projectIdentifier);
		// If user wasn't registered at webnode, we add identifier (update Db and session)
		if ($user->getWebnodeIdentifier() === null)
		{
			$user->setWebnodeIdentifier($response->getUserHash());
			$user = $this->userRepository->updateUser($user);
			$this->userLoginService->loginUser($user);
		}
		$this->clearProjectsCache($user);

		$this->setPackage($response->getProjectIdentifier(), 'limited');

		return $response->getRedirectUrl();
	}

	private function clearProjectsCache(User $user)
	{
		// reset projects cache, so nextime we pull new proejct
		$this->setCacheValue($user->getId() . '|projects', null);
	}
	
	public function authorizeWebnodeOAuth2(string $authorizationUrl, string $userIdentifier = null): string
	{
		return $this->oauth2Api->getAuthorizationUrlForUser(
			$userIdentifier ?? $this->userLoginService->getLoggedInUser()->getWebnodeIdentifier(),
			$authorizationUrl
		);
	}

	public function disableProject(string $identifier): void
	{
		$this->projectApi->update($identifier, Project::STATUS_DISABLED);
		$this->clearProjectsCache($this->userLoginService->getLoggedInUser());
	}

	public function enableProject(string $identifier): void
	{
		$this->projectApi->update($identifier, Project::STATUS_ENABLED);
		$this->clearProjectsCache($this->userLoginService->getLoggedInUser());
	}

	public function resetProject(string $identifier): void
	{
		$this->projectApi->update($identifier, Project::STATUS_INITIALIZED);
		$this->clearProjectsCache($this->userLoginService->getLoggedInUser());
	}

	public function deleteProject(string $identifier): void
	{
		$this->projectApi->delete($identifier);
		$this->clearProjectsCache($this->userLoginService->getLoggedInUser());
	}

	public function setPackage(string $identifier, ?string $newPackage): void
	{
		$this->projectApi->changePackage($identifier, $newPackage);
		$this->clearProjectsCache($this->userLoginService->getLoggedInUser());
	}

	public function setMasterDomain(string $identifier, string $domain): bool
	{
		$result = $this->projectApi->setMasterDomainToProject($identifier, $domain);
		$this->clearProjectsCache($this->userLoginService->getLoggedInUser());
		return $result;
	}
}
