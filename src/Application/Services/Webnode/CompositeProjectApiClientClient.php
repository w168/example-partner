<?php

declare(strict_types=1);

namespace App\Application\Services\Webnode;

use App\Application\Services\AuthorizedHttpClientWrapper;
use App\Application\Services\UserLoginService;
use App\Domain\User\UserRepository;
use App\Infrastructure\Persistence\SleekRepository;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use SleekDB\Exceptions\InvalidArgumentException;
use SleekDB\Exceptions\IOException;

/**
 * Temporary place until API is finnished, it uses the api AND it mocks some endpoints
 *
 */
final class CompositeProjectApiClientClient extends SleekRepository implements ProjectApiClientInterface
{
	private AuthorizedHttpClientWrapper $httpClient;

	private UserRepository $userRepository;

	private UserLoginService $userLoginService;
	
	/**
	 * @inheritDoc
	 */
	public function __construct(AuthorizedHttpClientWrapper $httpClient, UserRepository $userRepository, UserLoginService $userLoginService)
	{
		parent::__construct('projects');
		$this->httpClient = $httpClient;
		$this->userRepository = $userRepository;
		$this->userLoginService = $userLoginService;
	}
	
	
	/**
	 * @param string $userIdentifier
	 * @return string[]
	 * @throws IOException
	 * @throws InvalidArgumentException
	 */
	public function findByUserIdentifier(string $userIdentifier): array
	{
		$response = $this->httpClient->getClient()->request('GET', "/users/{$userIdentifier}/projects");
		$responseData = json_decode((string)$response->getBody(), true);
		return $responseData['data'];
	}
	
	
	public function delete(string $identifier): void
	{
		$this->httpClient->getClient()->request('DELETE', "/projects/{$identifier}");
	}

	public function update(string $identifier, string $status): void
	{
		$this->httpClient->getClient()->request('PATCH', "/projects/{$identifier}", [
			RequestOptions::JSON => [
				'status' => $status
			],
		]);
	}
	
	
	/**
	 * @param string $userEmail
	 * @param string|null $identifier
	 * @return NewProjectResponse
	 * @throws IOException
	 * @throws InvalidArgumentException
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 * @throws \SleekDB\Exceptions\IdNotAllowedException
	 * @throws \SleekDB\Exceptions\JsonException
	 *
	 */
	public function create(string $userEmail, string $identifier = null): NewProjectResponse
	{
		$identifier = $identifier ?? 'w'.bin2hex(random_bytes(10));
		// call an API
		$response = $this->httpClient->getClient()->request('POST', '/projects', [
			RequestOptions::FORM_PARAMS => [
				'projectIdentifierHint' => $identifier,
				'language' => 'en',
				'email' => $userEmail,
				'replyToEmail' => '',
			]
		]);
		$responseData = json_decode((string)$response->getBody(), true);
		
		$this->updateUserIdentifier($responseData['data']['adminUserIdentifier']);

		return (new NewProjectResponse())
			->setUserHash($responseData['data']['adminUserIdentifier'])
			->setProjectIdentifier($identifier)
			->setRedirectUrl($responseData['data']['cmsUrl']);
	}
	
	private function updateUserIdentifier(string $identifier)
	{
		$user = $this->userLoginService->getLoggedInUser();
		if (!$user->getWebnodeIdentifier())
		{
			$user->setWebnodeIdentifier($identifier);
			$user = $this->userRepository->updateUser($user);
			$this->userLoginService->loginUser($user);
		}
	}
	
	
	public function findByIdentifier(string $identifier): array
	{
		$response = $this->httpClient->getClient()->request('GET', "/projects/{$identifier}");
		$responseData = json_decode((string)$response->getBody(), true);
		return $responseData['data'];
	}

	public function getProjectPackages(string $identifier): array
	{
		$response = $this->httpClient->getClient()->request('GET', "/projects/{$identifier}/packages");
		$responseData = json_decode((string)$response->getBody(), true);
		return $responseData['data'];
	}

	public function changePackage(string $identifier, ?string $newPackage): ?string
	{
		$currentPackages = $this->getProjectPackages($identifier);
		foreach ($currentPackages as $package)
		{
			if ($package['isActive'] ?? false)
			{
				// neměníme na stejný package
				if ($newPackage === $package['identifier'])
				{
					return $newPackage;
				}

				$this->httpClient->getClient()->request('DELETE', "/projects/{$identifier}/packages");
				break;
			}
		}

		if (!empty($newPackage))
		{
			$response = $this->httpClient->getClient()->request('POST', "/projects/{$identifier}/packages", [
				RequestOptions::FORM_PARAMS => ['packageIdentifier' => $newPackage]
			]);
			return $newPackage;
		}
		return null;
	}

	public function setMasterDomainToProject(string $identifier, string $domain): bool
	{
		try
		{
			$currentDomainProjectIdentifier = $this->getCurrentProjectForDomain($domain);
			if ($currentDomainProjectIdentifier) {
				if ($identifier === $currentDomainProjectIdentifier) {
					$this->httpClient->getClient()->request('PATCH', "/domains/{$domain}");
					return true;
				}
				$this->httpClient->getClient()->request(
					'DELETE',
					"/projects/{$currentDomainProjectIdentifier}/domains/{$domain}"
				);
			}
			$this->httpClient->getClient()->request('POST', "/projects/{$identifier}/domains", [
				RequestOptions::FORM_PARAMS => ['domainName' => $domain]
			]);
			$this->httpClient->getClient()->request('POST', "/domains/{$domain}/certificate");
			$this->httpClient->getClient()->request('PATCH', "/domains/{$domain}");
			return true;
		}
		catch (\Exception $e)
		{
			return false;
		}
	}

	public function getProjectDomains(string $identifier): array
	{
		$response = $this->httpClient->getClient()->request('GET', "/projects/{$identifier}/domains");
		$responseData = json_decode((string)$response->getBody(), true);
		return $responseData['data'];
	}

	private function getCurrentProjectForDomain(string $domain): ?string
	{
		try {
			$response = $this->httpClient->getClient()->request('GET', "/domains/{$domain}");
			if ($response->getStatusCode() === 200)
			{
				$responseData = json_decode((string)$response->getBody(), true);
				return $responseData['data']['projectIdentifier'];
			}
		}
		catch (GuzzleException $e) {
			//nop
		}
		return null;
	}
}
