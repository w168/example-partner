<?php

declare(strict_types=1);

namespace App\Application\Services\Webnode;

interface ProjectApiClientInterface
{
	public function findByUserIdentifier(string $userIdentifier): array;
	public function findByIdentifier(string $identifier): array;

	public function getProjectPackages(string $identifier): array;
	public function getProjectDomains(string $identifier): array;

	public function create(string $userEmail, string $identifier = null): NewProjectResponse;
	public function delete(string $identifier): void;
	public function update(string $identifier, string $status): void;

	public function changePackage(string $identifier, string $newPackage): ?string;
	public function setMasterDomainToProject(string $identifier, string $domain): bool;
}
