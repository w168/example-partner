<?php

declare(strict_types=1);

namespace App\Application\Services\Webnode;

use webnode\oauth2\Service\OAuth2ApiInterface;

/**
 * Service for providing API authorization header
 */
final class ApiAuthorizationService
{
	private ?string $accessToken = null;

	private OAuth2ApiInterface $oAuth2Api;

	public function __construct(OAuth2ApiInterface $oAuth2Api)
	{
		$this->oAuth2Api = $oAuth2Api;
	}
	
	
	public function getAuthorizationHeader(): string
	{
		$token = $this->getAccessToken();
		
		return "Bearer $token";
	}
	
	
	private function getAccessToken(): string
	{
		if (!$this->accessToken)
		{
			$this->generateAccessToken();
		}
		
		return $this->accessToken;
	}
	
	
	private function generateAccessToken(): void
	{
		$tokens            = $this->oAuth2Api->clientCredentialsGrant();
		$this->accessToken = $tokens->getAccess();
	}
}
