<?php

declare(strict_types=1);

namespace App\Application\Services;

final class Log
{
	private static int $startTime;
	
	public static function start(): void
	{
		self::$startTime = self::msTime();
		self::log('start');
	}
	
	public static function log(string $message): void
	{
		$time = self::msTime() - self::$startTime;
		file_put_contents(self::getFileName(), "$time \"$message\"\n", FILE_APPEND);
	}
	
	
	private static function getFileName(): string
	{
		return __DIR__ . '/../../../logs/timeLog_' . self::$startTime;
	}
	
	private static function msTime(): int
	{
		return (int)round(microtime(true) * 1000);
	}
}
