<?php

declare(strict_types=1);

namespace App\Application\Services;



use App\Application\Services\Webnode\ApiAuthorizationService;
use GuzzleHttp\Client;

final class AuthorizedHttpClientWrapper
{
	private Client $client;
	
	private array $settings;
	
	private ApiAuthorizationService $authService;
	
	
	public function __construct(array $settings, ApiAuthorizationService $authService)
	{
		$this->settings    = $settings;
		$this->authService = $authService;
	}
	
	
	public function getClient(): Client
	{
		if (empty($this->client))
		{
			$authorizedSettings = $this->settings;
			$authorizedSettings['headers']['Authorization'] = $this->authService->getAuthorizationHeader();
			$this->client =  new \GuzzleHttp\Client($authorizedSettings);
		}
		return $this->client;
	}
}
