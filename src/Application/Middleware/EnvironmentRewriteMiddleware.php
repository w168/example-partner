<?php
declare(strict_types=1);

namespace App\Application\Middleware;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\MiddlewareInterface as Middleware;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Psr7\Stream;

/**
 * Simple/Dumb way to enable multiple environments
 */
class EnvironmentRewriteMiddleware implements Middleware
{
	private ?string $envPathPrefix = null;


    /**
     * {@inheritdoc}
     */
    public function process(Request $request, RequestHandler $handler): Response
    {
		$request = $this->overwriteEnv($request);
        $response = $handler->handle($request);
		return $this->addEnvironmentToUrls($request, $response);
    }

	/**
	 * Overwrites api url and removes environment from request path
	 */
	private function overwriteEnv(Request $request): Request
	{
		if (!empty($_ENV['API_PRE_ENV_KEY']) && !empty($_ENV['ALLOWED_DEV_ENVIRONMENTS']))
		{
			$strData = $_ENV['ALLOWED_DEV_ENVIRONMENTS'];
			$allowedEnvironments = json_decode($strData);
			if (is_array($allowedEnvironments))
			{
				$uriPath = $request->getUri()->getPath();
				foreach ($allowedEnvironments as $environment)
				{
					if (str_starts_with($uriPath, '/' . $environment))
					{
						$_ENV['API_GATEWAY_URL'] = str_replace(
							$_ENV['API_PRE_ENV_KEY'],
							$_ENV['API_PRE_ENV_KEY'] . $environment .'.',
							$_ENV['API_GATEWAY_URL']
						);

						$request = $request->withUri(
							$request
								->getUri()
								->withPath(str_replace('/' . $environment, '', $uriPath))
						);
						$this->envPathPrefix = '/' . $environment;
						break;
					}
				}
			}
		}
		return $request;
	}

	/**
	 * Ands /environment prefix to all url paths
	 */
	private function addEnvironmentToUrls(Request $request, Response $response): Response
	{
		$response->getBody()->rewind();
		$strResponseBody = $response->getBody()->getContents();
		if ($this->envPathPrefix !== null)
		{
			$host = $request->getUri()->getHost();
			if (!empty($strResponseBody))
			{
				$strResponseBody = str_replace(
					[$host, 'href="/', 'action="/'],
					[
						$host . $this->envPathPrefix,
						'href="' . $this->envPathPrefix . '/',
						'action="' . $this->envPathPrefix . '/'
					],
					$strResponseBody
				);
				$response = $response->withBody(new Stream(fopen('php://temp', 'r+')));
				$response->getBody()->write($strResponseBody);
			}
			$location = $response->getHeader('Location');
			if (!empty($location))
			{
				$location = $location[0];
				if (str_starts_with($location, '/'))
				{
					$response = $response->withHeader('Location', $this->envPathPrefix . $location);
				}
				else
				{
					$response = $response->withHeader('Location', str_replace(
						$host,
						$host . $this->envPathPrefix,
						$location
					));
				}
			}

		}
		return $response;
	}
}
