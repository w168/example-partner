<?php

declare(strict_types=1);

namespace App\Application\Actions\Users;

use App\Application\Actions\Action;
use App\Application\Services\UserLoginService;
use App\Domain\User\UserPermissions;
use App\Domain\User\UserRepository;
use Psr\Log\LoggerInterface;
use Slim\Views\Twig;

abstract class UsersAction extends Action
{
	protected UserLoginService $userLoginService;

	protected UserRepository $userRepository;

	protected Twig $twig;

	public function __construct(
		UserLoginService $userLoginService,
		UserRepository $userRepository,
		Twig $twig,
		LoggerInterface $logger
	)
	{
		parent::__construct($logger);
		$this->userLoginService = $userLoginService;
		$this->userRepository = $userRepository;
		$this->twig = $twig;
	}
	
	protected function isAccessAllowed(): bool
	{
		$user = $this->userLoginService->getLoggedInUser();
		return $user && $user->getPermissions()->hasPermission(UserPermissions::USERS);
	}
}
