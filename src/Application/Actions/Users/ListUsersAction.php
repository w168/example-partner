<?php

declare(strict_types=1);

namespace App\Application\Actions\Users;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpUnauthorizedException;

final class ListUsersAction extends UsersAction
{
	protected function action(): Response
	{
		if (!$this->isAccessAllowed())
		{
			return $this->respondWithRedirect('/login');
		}
		throw new HttpUnauthorizedException($this->request);
	}
}
