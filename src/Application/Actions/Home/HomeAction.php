<?php

declare(strict_types=1);

namespace App\Application\Actions\Home;

use App\Application\Actions\Action;
use App\Application\Services\Log;
use App\Application\Services\UserLoginService;
use App\Application\Services\Webnode\ProjectApiClientInterface;
use App\Application\Services\Webnode\WebnodeFacade;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Views\Twig;

final class HomeAction extends Action
{
	private UserLoginService $userLoginService;

	private Twig $twig;

	private WebnodeFacade $webnode;

	public function __construct(
		UserLoginService $userLoginService,
		Twig $twig,
		WebnodeFacade $webnodeFacade,
		LoggerInterface $logger
	) {
		parent::__construct($logger);
		$this->userLoginService = $userLoginService;
		$this->twig = $twig;
		$this->webnode = $webnodeFacade;
	}
	
	
	/**
	 * {@inheritdoc}
	 */
	protected function action(): Response
	{
		$user = $this->userLoginService->getLoggedInUser();
		
		if ($user === null)
		{
			// todo url ze servisy (chce to app a to ted nemam)
			return $this->respondWithRedirect('/login');
		}
		
		$projects = $this->webnode->getUserProjects($user);
		
		return $this->twig->render($this->response, 'home.twig', [
			'user' => $user,
			'projects' => $projects,
		]);
	}
}
