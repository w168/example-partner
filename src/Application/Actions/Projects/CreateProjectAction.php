<?php

declare(strict_types=1);

namespace App\Application\Actions\Projects;

use App\Application\Actions\Action;
use App\Application\Services\Webnode\WebnodeFacade;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Views\Twig;

final class CreateProjectAction extends Action
{
	private WebnodeFacade $webnode;
	private Twig $twig;
	private string $preSharedKey;


	public function __construct(
		LoggerInterface $logger,
		WebnodeFacade $webnode,
		Twig $twig,
		string $preSharedKey
	)  {
		parent::__construct($logger);
		$this->webnode = $webnode;
		$this->twig             = $twig;
		$this->preSharedKey = $preSharedKey;
	}
	
	
	/**
	 * {@inheritdoc}
	 */
	protected function action(): Response
	{
		$errors = [];
		if ($this->request->getMethod() === 'POST')
		{
			$body = $this->request->getParsedBody();
			$errors = $this->getFormErrors($body);
			
			if (empty($errors))
			{
				$redirectUri = $this->webnode->createProject($body['identifier'] ?? null);

				// currently partners redirect here based on user action, which takes longer than "instant"
				sleep(1);

				// depending on authorization flow, you can now respond with authorized or normal redirect
				return $this->respondWithRedirect($this->webnode->authorizeWebnodeOAuth2($redirectUri));
			}
			
			
		}
		return $this->twig->render($this->response, 'createProject.twig', ['errors' => $errors]);
	}

	private function getFormErrors(array $body)
	{
		if ($body['preSharedKey'] !== $this->preSharedKey)
		{
			return [
				'preSharedKey' => 'Invalid key'
			];
		}
		return [];
	}
}
