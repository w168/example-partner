<?php

declare(strict_types=1);

namespace App\Application\Actions\EditProject;

use App\Application\Actions\Action;
use App\Application\Services\UserLoginService;
use App\Application\Services\Webnode\WebnodeFacade;
use App\Domain\Project;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Views\Twig;

final class EditProjectAction extends Action
{
	private UserLoginService $userLoginService;

	private Twig $twig;

	private WebnodeFacade $webnode;

	private bool $error = false;

	public function __construct(
		UserLoginService $userLoginService,
		Twig $twig,
		WebnodeFacade $webnodeFacade,
		LoggerInterface $logger
	) {
		parent::__construct($logger);
		$this->userLoginService = $userLoginService;
		$this->twig = $twig;
		$this->webnode = $webnodeFacade;
	}
	
	
	/**
	 * {@inheritdoc}
	 */
	protected function action(): Response
	{
		$this->error = $_SESSION['edit_project_error'] ?? false;
		unset($_SESSION['edit_project_error']);
		$user = $this->userLoginService->getLoggedInUser();

		if ($user === null)
		{
			// todo url ze servisy (chce to app a to ted nemam)
			return $this->respondWithRedirect('/login');
		}

		$project = $this->webnode->getUserProject($this->args['identifier']);
		if (!$project)
		{
			return $this->respondWithRedirect('/');
		}

		if ($this->request->getMethod() === 'POST')
		{
			$postResponse = $this->processPostAction($project);
			if ($postResponse)
			{
				return $postResponse;
			}

			$_SESSION['edit_project_error'] = $this->error;
			return $this->respondWithRedirect((string)$this->request->getUri());
		}

		return $this->twig->render($this->response, 'editProject.twig', [
			'packages' => Project::PACKAGES,
			'project' => $project,
			'error' => $this->error,
		]);
	}

	private function processPostAction(Project $project): ?Response
	{
		$body = $this->request->getParsedBody();
		$this->error = false;
		switch ($body['action'])
		{
			case 'delete':
				$this->webnode->deleteProject($project->getIdentifier());
				return $this->respondWithRedirect('/');
			case 'disable':
				$this->webnode->disableProject($project->getIdentifier());
				return null;
			case 'enable':
				$this->webnode->enableProject($project->getIdentifier());
				return null;
			case 'reset':
				$this->webnode->resetProject($project->getIdentifier());
				return $this->respondWithRedirect($this->webnode->authorizeWebnodeOAuth2($project->getCmsUrl()));
			case 'setPackage':
				$this->webnode->setPackage($project->getIdentifier(), $body['package'] ?: null);
				return null;
			case 'setDomain':
				$this->error = !$this->webnode->setMasterDomain($project->getIdentifier(), $body['domain']);

				return null;
			default:
				return null;
		}
	}
}
