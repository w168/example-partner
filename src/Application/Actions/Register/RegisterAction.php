<?php

declare(strict_types=1);

namespace App\Application\Actions\Register;

use App\Application\Actions\Action;
use App\Application\Services\UserLoginService;
use App\Domain\User\User;
use App\Domain\User\UserPermissions;
use App\Domain\User\UserRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Views\Twig;

final class RegisterAction extends Action
{
	private UserLoginService $userLoginService;

	private Twig $twig;

	private UserRepository $userRepository;

	public function __construct(
		UserLoginService $userLoginService,
		UserRepository $userRepository,
		Twig $twig,
		LoggerInterface $logger
	)
	{
		parent::__construct($logger);
		$this->userLoginService = $userLoginService;
		$this->userRepository = $userRepository;
		$this->twig = $twig;
	}
	
	
	/**
	 * {@inheritdoc}
	 */
	protected function action(): Response
	{
		$error = null;
		if ($this->request->getMethod() === 'POST')
		{
			$body = $this->request->getParsedBody();
			$query = $this->request->getQueryParams();
			$user = $this->userRepository->insertUser(
				new User(
					null,
					$body['email']
					, password_hash($body['password'],PASSWORD_BCRYPT),
					new UserPermissions([]),
					 null
				)
			);
			$this->userLoginService->loginUser($user);
			return $this->respondWithRedirect($query['redirect_uri'] ?? '/');
		}
		return $this->twig->render($this->response, 'register.twig', ['error' => $error]);
	}
}
