<?php

declare(strict_types=1);

namespace App\Application\Actions\Authorize;

use App\Application\Actions\Action;
use App\Application\Services\Log;
use App\Application\Services\UserLoginService;
use App\Application\Services\Webnode\WebnodeFacade;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Psr7\Uri;
use Slim\Views\Twig;
use webnode\oauth2\Entity\AuthorizationErrorEnum;
use function _PHPStan_76800bfb5\RingCentral\Psr7\str;

final class AuthorizeAction extends Action
{
	private UserLoginService $userLoginService;
	private WebnodeFacade $webnodeFacade;

	public function __construct(
		UserLoginService $userLoginService,
		LoggerInterface $logger,
		WebnodeFacade $webnodeFacade
	)
	{
		parent::__construct($logger);
		$this->userLoginService = $userLoginService;
		$this->webnodeFacade = $webnodeFacade;
	}
	
	
	/**
	 * {@inheritdoc}
	 */
	protected function action(): Response
	{
		$uri = $this->request->getUri();
		$query = $this->request->getQueryParams();
		if(isset($query['logout']))
		{
			$this->userLoginService->logout();
			return $this->respondWithRedirect('/login');
		}
		
		if (!$this->userLoginService->getLoggedInUser())
		{
			return $this->respondWithRedirect('/login?redirect_uri='. urlencode((string)$uri));
		}
		
		if (isset($query['error']))
		{
			if ($query['error'] !== AuthorizationErrorEnum::APP_UNAUTHORIZED)
			{
				// todo handle errors
				return $this->respondWithData(['error' => $query['error']], 403);
			}
			elseif (isset($query['project_identifier']))
			{
				if (isset($_SESSION['unauthorizedVisit'][$query['project_identifier']]))
				{
					return $this->respondWithRedirect('/login?logout=1');
				}
				$_SESSION['unauthorizedVisit'][$query['project_identifier']] = true;
			}
		}
		
		if(isset($query['success']))
		{
			return $this->respondWithRedirect($query['redirect_uri'] ?? '/');
		}

		if (isset($query['forward_authorized']))
		{
			$dest = $this->webnodeFacade->authorizeWebnodeOAuth2($query['forward_authorized']);
			return $this->respondWithRedirect($dest);
		}
		
		$redirectUri = new Uri(
			'https',
			$uri->getHost(),
			null, // todo pass port (currently turned off due to ngrok issues)
			$uri->getPath()
		);
		if (!empty($query['redirect_uri']))
		{
			$redirectUri = $redirectUri->withQuery('redirect_uri='.urlencode($query['redirect_uri']));
		}
		
		$dest = $this->webnodeFacade->authorizeWebnodeOAuth2((string)($redirectUri));
		
		return $this->respondWithRedirect($dest);
	}
}
