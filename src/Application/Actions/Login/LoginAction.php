<?php

declare(strict_types=1);

namespace App\Application\Actions\Login;

use App\Application\Actions\Action;
use App\Application\Services\UserLoginService;
use App\Domain\User\UserNotFoundException;
use App\Domain\User\UserRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Psr7\Uri;
use Slim\Views\Twig;
use webnode\oauth2\Service\OAuth2UrlServiceInterface;

final class LoginAction extends Action
{
	private UserLoginService $userLoginService;

	protected UserRepository $userRepository;

	private Twig $twig;

	private OAuth2UrlServiceInterface $urlService;

	public function __construct(
		UserLoginService $userLoginService,
		UserRepository $userRepository,
		OAuth2UrlServiceInterface $urlService,
		Twig $twig,
		LoggerInterface $logger
	)
	{
		parent::__construct($logger);
		$this->userLoginService = $userLoginService;
		$this->userRepository = $userRepository;
		$this->twig = $twig;
		$this->urlService = $urlService;
	}
	
	
	/**
	 * {@inheritdoc}
	 */
	protected function action(): Response
	{
		$query = $this->request->getQueryParams();
		$error = null;
		if ($this->request->getMethod() === 'POST')
		{
			$body = $this->request->getParsedBody();
			try
			{
				$user = $this->userRepository->findUserByEmail($body['email']);
				$valid = password_verify($body['password'], $user->getPassword());
			}
			catch (UserNotFoundException $exception)
			{
				$user = null;
				$valid = false;
			}
			if ($valid)
			{
				$this->userLoginService->loginUser($user);
				return $this->respondWithRedirect($query['redirect_uri'] ?? '/');
			}
			$error = 'Invalid Name Or Password';
		}
		elseif (isset($query['logout']) && $this->userLoginService->getLoggedInUser() !== null)
		{
			$this->userLoginService->logout();
			$uri = $this->request->getUri();
			return $this->respondWithRedirect((string)$this->urlService->getSSOutUrl(
				(string)(new Uri('https', $uri->getHost(), null, '/authorize'))
			));
		}
		return $this->twig->render($this->response, 'login.twig', ['error' => $error]);
	}
}
