<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\User;

use App\Domain\User\User;
use App\Domain\User\UserNotFoundException;
use App\Domain\User\UserRepository;
use App\Infrastructure\Persistence\SleekRepository;
use SleekDB\Exceptions\IdNotAllowedException;
use SleekDB\Exceptions\InvalidArgumentException;
use SleekDB\Exceptions\IOException;
use SleekDB\Exceptions\JsonException;

final class SleekUserRepository extends SleekRepository implements UserRepository
{
	/**
	 * @inheritDoc
	 */
	public function __construct()
	{
		parent::__construct('users');
	}
	
	
	/**
	 * @return User[]
	 * @throws IOException
	 * @throws InvalidArgumentException
	 */
	public function findAll(): array
	{
		$data = $this->store->findAll();
		$users = [];
		foreach ($data as $userData)
		{
			$users[] = User::fromArray($userData);
		}
		return $users;
	}
	
	
	/**
	 * @param int $id
	 * @return User
	 * @throws InvalidArgumentException
	 *
	 */
	public function findUserById(int $id): User
	{
		$userData = $this->store->findById($id);
		return User::fromArray($userData);
	}
	
	
	/**
	 * @param string $email
	 * @return User
	 * @throws IOException
	 * @throws InvalidArgumentException
	 * @throws UserNotFoundException
	 *
	 */
	public function findUserByEmail(string $email): User
	{
		$userData = $this->store->findOneBy(['email', '=' ,$email]);
		if ($userData === null)
		{
			throw new UserNotFoundException();
		}
		return User::fromArray($userData);
	}
	
	
	/**
	 * @param User $user
	 * @return User
	 * @throws IOException
	 * @throws IdNotAllowedException
	 * @throws InvalidArgumentException
	 * @throws JsonException
	 *
	 */
	public function insertUser(User $user): User
	{
		$userData = $this->store->insert($user->toArray());
		return User::fromArray($userData);
	}
	
	
	/**
	 * @param User $user
	 * @return User
	 * @throws IOException
	 * @throws InvalidArgumentException
	 * @throws JsonException
	 *
	 */
	public function updateUser(User $user): User
	{
		$data = $user->toArray();
		unset($data['_id']);
		$userData = $this->store->updateById($user->getId(),$data);
		return User::fromArray($userData);
	}
}
