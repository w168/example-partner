<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence;

use SleekDB\Exceptions\InvalidArgumentException;
use SleekDB\Exceptions\InvalidConfigurationException;
use SleekDB\Exceptions\IOException;
use SleekDB\Store;

abstract class SleekRepository
{
	private $databaseDirectory = __DIR__ . "/../../../database";

	protected Store $store;
	
	
	/**
	 * @param string $dbName
	 * @throws IOException
	 * @throws InvalidArgumentException
	 * @throws InvalidConfigurationException
	 */
	public function __construct(string $dbName)
	{
		$this->store = new Store($dbName, $this->databaseDirectory, ['timeout' => false, 'cache_lifetime' => 60]);
	}
	
}
