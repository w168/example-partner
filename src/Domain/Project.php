<?php

declare(strict_types=1);

namespace App\Domain;

final class Project
{
	public const STATUS_ENABLED = 'ENABLED';
	public const STATUS_DISABLED = 'DISABLED';
	public const STATUS_INITIALIZED = 'INITIALIZED';

	public const PACKAGE_LIMITED = 'limited';
	public const PACKAGE_MINI = 'mini';
	public const PACKAGE_STANDARD = 'standard';
	public const PACKAGE_PROFESSIONAL = 'professional';
	public const PACKAGE_BUSINESS = 'business';

	public const PACKAGES = [
		self::PACKAGE_LIMITED,
		self::PACKAGE_MINI,
		self::PACKAGE_STANDARD,
		self::PACKAGE_PROFESSIONAL,
		self::PACKAGE_BUSINESS,
	];


	private string $identifier;

	private string $userIdentifier;

	private ?string $domainName;

	private ?string $package;

	private string $cmsUrl;

	private string $portalUrl;

	private string $frontendUrl;

	private string $status;

	private string $ip;
	private ?string $masterDomain;

	/**
	 * @var Array<int, string>
	 */
	private array $otherDomains;

	public function __construct(
		string $identifier,
		string $userIdentifier,
		?string $domainName,
		?string $package,
		string $cmsUrl,
		string $portalUrl,
		string $frontendUrl,
		string $status,
		string $ip,
		?string $masterDomain,
		array $otherDomains
	) {
		$this->identifier = $identifier;
		$this->userIdentifier = $userIdentifier;
		$this->domainName = $domainName;
		$this->package = $package;
		$this->cmsUrl = $cmsUrl;
		$this->portalUrl = $portalUrl;
		$this->frontendUrl = $frontendUrl;
		$this->status = $status;
		$this->ip = $ip;
		$this->masterDomain = $masterDomain;
		$this->otherDomains = $otherDomains;
	}

	public function getIdentifier(): string
	{
		return $this->identifier;
	}

	public function getUserIdentifier(): string
	{
		return $this->userIdentifier;
	}

	public function getDomainName(): ?string
	{
		return $this->domainName;
	}

	public function getPackage(): ?string
	{
		return $this->package;
	}

	public function getCmsUrl(): string
	{
		return $this->cmsUrl;
	}

	public function getPortalUrl(): string
	{
		return $this->portalUrl;
	}

	public function getFrontendUrl(): string
	{
		return $this->frontendUrl;
	}

	public function getStatus(): string
	{
		return $this->status;
	}

	public function getIp(): string
	{
		return $this->ip;
	}

	public function getMasterDomain(): ?string
	{
		return $this->masterDomain;
	}

	/**
	 * @return Array<int, string>
	 */
	public function getOtherDomains(): array
	{
		return $this->otherDomains;
	}
}
