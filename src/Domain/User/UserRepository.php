<?php
declare(strict_types=1);

namespace App\Domain\User;

interface UserRepository
{
    /**
     * @return User[]
     */
    public function findAll(): array;

    /**
     * @param int $id
     * @return User
     * @throws UserNotFoundException
     */
    public function findUserById(int $id): User;

    /**
     * @param string $email
     * @return User
     * @throws UserNotFoundException
     */
    public function findUserByEmail(string $email): User;
    
	/**
	 * @param User $user
	 * @return User
	 *
	 */
    public function insertUser(User $user): User;
    
	/**
	 * @param User $user
	 * @return User
	 *
	 */
    public function updateUser(User $user): User;
}
