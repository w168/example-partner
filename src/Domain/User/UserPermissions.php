<?php

declare(strict_types=1);

namespace App\Domain\User;

final class UserPermissions implements \JsonSerializable
{
	const USERS = 'USERS';
	
	const ALL = [self::USERS];
	
	private $current = [];
	
	
	/**
	 * @param string[] $current
	 */
	public function __construct(array $current)
	{
		$this->current = $current;
	}
	
	public function hasPermission(string $permission): bool
	{
		return in_array($permission, $this->current);
	}
	
	public static function fromArray(array $data): UserPermissions
	{
		return new UserPermissions($data);
	}
	
	public function toArray()
	{
		return $this->current;
	}
	
	
	public function jsonSerialize(): array
	{
		return $this->toArray();
	}
}
