<?php
declare(strict_types=1);

namespace App\Domain\User;

use JsonSerializable;

class User implements JsonSerializable
{
    private ?int $id;

    private string $email;

    private string $password;

    private ?string $webnodeIdentifier;

	private UserPermissions $permissions;
	
	

	public function __construct(
		?int $id,
		string $email,
		string $password,
		UserPermissions $permissions,
		?string $webnodeIdentifier = null
	) {
		$this->id       = $id;
		$this->email = $email;
		$this->password = $password;
		$this->permissions = $permissions;
		$this->webnodeIdentifier = $webnodeIdentifier;
	}
	

	public function getId(): ?int
	{
		return $this->id;
	}
	

	public function getEmail(): string
	{
		return $this->email;
	}
	

	public function getPassword(): string
	{
		return $this->password;
	}

	public function getPermissions(): UserPermissions
	{
		return $this->permissions;
	}

	public function getWebnodeIdentifier(): ?string
	{
		return $this->webnodeIdentifier;
	}
	

	public function setWebnodeIdentifier(string $webnodeIdentifier): void
	{
		$this->webnodeIdentifier = $webnodeIdentifier;
	}
	
	
	/**
	 * @param mixed[] $data
	 * @return User
	 */
	public static function fromArray(array $data): User
	{
		return new User(
			$data['_id'],
			$data['email'],
			$data['password'],
			UserPermissions::fromArray($data['permissions'] ?? []),
			$data['webnodeIdentifier'] ?? null,
		);
	}
	
	/**
	 * @return mixed[]
	 */
	public function toArray(): array
	{
		return [
			'_id' => $this->id,
			'email' => $this->email,
			'password' => $this->password,
			'permissions' => $this->permissions->toArray(),
			'webnodeIdentifier' => $this->webnodeIdentifier,
		];
	}
	
	/**
     * @return mixed[]
     */
    public function jsonSerialize(): array
	{
        return $this->toArray();
    }
}
