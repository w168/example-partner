# Partner Example

To run the application in development, you can run these commands 

```bash
cd [repository folder]
composer install
composer start
```

After that, open `http://127.0.0.1:3000` in your browser.

for SSL testing, I prefer to use ngrok tunnel:
https://dashboard.ngrok.com/get-started/setup 

Run this command in the application directory to run the test suite

```bash
composer test
```

