<?php
declare(strict_types=1);

use App\Application\Actions\Authorize\AuthorizeAction;
use App\Application\Actions\EditProject\EditProjectAction;
use App\Application\Actions\Home\HomeAction;
use App\Application\Actions\Login\LoginAction;
use App\Application\Actions\Projects\CreateProjectAction;
use App\Application\Actions\Register\RegisterAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->get('/', HomeAction::class)->setName('home');
    
    $app->get('/login', LoginAction::class)->setName('login');
	$app->post('/login', LoginAction::class);
	
	$app->get('/register', RegisterAction::class)->setName('register');
	$app->post('/register', RegisterAction::class);
	
	$app->get('/authorize', AuthorizeAction::class)->setName('authorize');

	$app->group('/projects', function (Group $group) {
		$group->get('/create', CreateProjectAction::class)->setName('createProject');
		$group->post('/create', CreateProjectAction::class);
		$group->get('/edit/{identifier}', EditProjectAction::class)->setName('editProject');
		$group->post('/edit/{identifier}', EditProjectAction::class);
	});
};
