<?php
declare(strict_types=1);

use App\Application\Actions\Projects\CreateProjectAction;
use App\Application\Services\AuthorizedHttpClientWrapper;
use App\Application\Services\UserLoginService;
use App\Application\Services\Webnode\ApiAuthorizationService;
use App\Application\Services\Webnode\CompositeProjectApiClientClient;
use App\Application\Services\Webnode\ProjectApiClientInterface;
use App\Application\Services\Webnode\WebnodeConfiguration;
use App\Application\Settings\SettingsInterface;
use App\Domain\User\UserRepository;
use DI\ContainerBuilder;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
		LoggerInterface::class => function (ContainerInterface $c) {
            $settings = $c->get(SettingsInterface::class);

            $loggerSettings = $settings->get('logger');
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },
		
		AuthorizedHttpClientWrapper::class => function (ContainerInterface $c) {
    		return new AuthorizedHttpClientWrapper(
				[
					'base_uri' => $_ENV['API_GATEWAY_URL'],
					'verify' => false,
				],
				$c->get(ApiAuthorizationService::class)
			);
		},
		
		ProjectApiClientInterface::class => function (ContainerInterface $c) {
			return new CompositeProjectApiClientClient(
				$c->get(AuthorizedHttpClientWrapper::class),
				$c->get(UserRepository::class),
				$c->get(UserLoginService::class)
			);
		},
	
		WebnodeConfiguration::class => function (ContainerInterface $c) {
			return new WebnodeConfiguration(
				// todo genenv does not work for some reason
				$_ENV['OAUTH2_PUBLIC'],
				$_ENV['OAUTH2_SECRET'],
				$_ENV['PORTAL_AUTHORIZATION_URL'],
				$_ENV['OAUTH2_BASE_URL']
			);
		},
	
		\webnode\oauth2\Configuration::class => function (ContainerInterface $c) {
    		/** @var WebnodeConfiguration $webnodeConfiguration */
    		$webnodeConfiguration = $c->get(WebnodeConfiguration::class);
    		return new \webnode\oauth2\Configuration(
				$webnodeConfiguration->getPublic(),
				$webnodeConfiguration->getSecret(),
				$webnodeConfiguration->getOauth2AuthorizationUrl()
			);
		},
	
		\webnode\oauth2\Factory\WebnodeOAuth2Factory::class => function (ContainerInterface $c) {
			$guzzleConf = [];
			if ($_ENV['ENVIRONMENT'] === 'DEV')
			{
				$guzzleConf['verify'] = false;
			}
    		return new \webnode\oauth2\Factory\WebnodeOAuth2Factory(
    			$c->get(\webnode\oauth2\Configuration::class),
				$guzzleConf
			);
		},
	
		\webnode\oauth2\Service\OAuth2ApiInterface::class => function (ContainerInterface $c) {
    		/** @var \webnode\oauth2\Factory\WebnodeOAuth2Factory $factory */
    		$factory = $c->get(\webnode\oauth2\Factory\WebnodeOAuth2Factory::class);
    		return $factory->createApi();
		},
	
		\webnode\oauth2\Service\OAuth2UrlServiceInterface::class => function (ContainerInterface $c) {
    		/** @var \webnode\oauth2\Factory\WebnodeOAuth2Factory $factory */
    		$factory = $c->get(\webnode\oauth2\Factory\WebnodeOAuth2Factory::class);
    		return $factory->createUrlGenerator();
		},

		CreateProjectAction::class => function (ContainerInterface $container) {
			return new CreateProjectAction(
				$container->get(LoggerInterface::class),
				$container->get(\App\Application\Services\Webnode\WebnodeFacade::class),
				$container->get(\Slim\Views\Twig::class),
				$_ENV['PRE_SHARED_KEY'] ?? $_ENV['INSTALLATION_KEY']
			);
		}
    ]);
};
