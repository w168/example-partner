<?php
declare(strict_types=1);

use App\Application\Middleware;
use Slim\App;
use Slim\Views\TwigMiddleware;

return function (App $app) {
    $app->add(Middleware\SessionMiddleware::class);
    $app->add(TwigMiddleware::createFromContainer($app, \Slim\Views\Twig::class));
	$app->add(Middleware\EnvironmentRewriteMiddleware::class);
};
