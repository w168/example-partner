<?php
declare(strict_types=1);

use App\Application\Settings\Settings;
use App\Application\Settings\SettingsInterface;
use DI\ContainerBuilder;
use Monolog\Logger;
use Slim\Psr7\Environment;
use Slim\Psr7\Uri;
use Slim\Views\Twig;

return function (ContainerBuilder $containerBuilder) {

    // Global Settings Object
    $containerBuilder->addDefinitions([
		SettingsInterface::class => function () {
            return new Settings([
                'displayErrorDetails' => true, // Should be set to false in production
                'logError'            => false,
                'logErrorDetails'     => false,
                'logger' => [
                    'name' => 'slim-app',
                    'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
                    'level' => Logger::DEBUG,
                ],
            ]);
        },
		Twig::class              => function ($container) {
    		$settings = [];
    		if ($_ENV['ENVIRONMENT'] === 'PROD')
			{
				$settings['cache'] = __DIR__ . '/../cache/twig';
			}
			$view = Twig::create(__DIR__ . '/../templates', $settings);
			return $view;
		}
    ]);
};
